set nocompatible
let &runtimepath = join(glob(stdpath('config') . "/bundle/*",0,1) + [&runtimepath],",")
set mouse=a
set number
set autoindent smartindent smarttab expandtab shiftwidth=2
set wildmenu wildmode=list:longest
set background=dark
silent! set termguicolors
silent! let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
silent! let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
colorscheme breezy

let g:deoplete#enable_at_startup = 1

let g:neomake_open_list = 2
let g:neomake_error_sign = {'text': 'E', 'texthl': 'NeomakeErrorSign'}
let g:neomake_warning_sign = {
    \   'text': 'W',
    \   'texthl': 'NeomakeWarningSign',
    \ }
let g:neomake_message_sign = {
     \   'text': '>',
     \   'texthl': 'NeomakeMessageSign',
     \ }
let g:neomake_info_sign = {'text': 'ℹ', 'texthl': 'NeomakeInfoSign'}

augroup my_neomake_signs
  au!
  autocmd ColorScheme *
        \ hi NeomakeErrorSign guifg=red guibg=#31363b |
        \ hi NeomakeWarningSign guifg=yellow guibg=#31363b |
        \ hi NeomakeMessageSign guifg=white guibg=#31363b
augroup END

call neomake#configure#automake('w')
